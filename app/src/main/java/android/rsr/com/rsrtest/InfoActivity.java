package android.rsr.com.rsrtest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
/**
 * @author Idriss El Baze
 * @version 0.1
 * @date 27/01/2018
 */
public class InfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
    }

    public void btnBack(View btn) {
        finish();
    }
}
