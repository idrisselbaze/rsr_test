package android.rsr.com.rsrtest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.Timer;
import java.util.TimerTask;
/**
 * @author Idriss El Baze
 * @version 0.1
 * @date 27/01/2018
 */
public class SplashActivity extends AppCompatActivity {
    //Showtime splashscreen milliseconds
    private int showTime = 1000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

    }

    // Add splash Tasker
    class SplashTask extends TimerTask {

        @Override
        public void run() {
            // Call main methode to start WelcomeActivity.class
            SplashActivity.this.showMain();
        }
    }


    private void showMain() {
        Intent intent = new Intent();
        intent.setClass(this, WelcomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Create and add Timer task
        new Timer().schedule(new SplashTask(), (long) this.showTime);


    }
}
