package android.rsr.com.rsrtest;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Toast;

/**
 * @author Idriss El Baze
 * @version 0.1
 * @date 27/01/2018
 */
public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){

        }else{
            buildAlertMessageNoGps();
        }
    }

    //Ask to enable GPS
    private void buildAlertMessageNoGps() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, android.R.style.Theme_DeviceDefault_Dialog_Alert);
        alertDialogBuilder.setTitle(R.string.error_gps_disabled_title);
        alertDialogBuilder.setMessage(R.string.error_gps_disabled_message)
                .setCancelable(false)
                .setNegativeButton(R.string.error_cancel,
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        dialog.cancel();
                        finish();
                    }
                });
        alertDialogBuilder.setPositiveButton(R.string.error_go_to_settings,
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        Intent callGPSSettingIntent = new Intent(
                                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(callGPSSettingIntent);
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
    //open map activity
    public void btnMapClick(View btn) {
        Intent mapintent = new Intent(WelcomeActivity.this, MapsActivity.class);
        startActivity(mapintent);
    }


    //open info activity
    public void btnInfoClick(View btn) {
        startActivity(new Intent(this, InfoActivity.class));
    }

}
